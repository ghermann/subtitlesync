Simple tool for synchronizing .srt subtitles.
Build it using stack (http://docs.haskellstack.org/en/stable/README.html).

Usage:
subtitlesync [fileName] [fromRate] [toRate]

Subtitle times be scaled with (fromRate / toRate).