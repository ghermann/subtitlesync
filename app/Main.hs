module Main where

import Lib
import SubtitleConverter
import System.Environment

main :: IO ()
main = do
    args <- getArgs
    let fileName = args !! 0
    let from = args !! 1
    let to = args !! 2
    mainWithArgs fileName (read from) (read to)
