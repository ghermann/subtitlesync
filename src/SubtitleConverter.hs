module SubtitleConverter where

import Text.Regex.Posix
import Text.Printf

mainWithArgs :: String -> Double -> Double -> IO ()
mainWithArgs fileName from to = do
    file <- readFile fileName
    writeFile (fileName ++ "conv.srt") $ unlines $ scale (from / to) $ lines file

timeRegex = "[0-9][0-9]:[0-9][0-9]:[0-9][0-9],[0-9][0-9][0-9]"
regex = timeRegex ++ " --> " ++ timeRegex

examp = "00:00:01,785 --> 00:00:04,108"

from25to23976 = 23.976/25
from25to24 = 23.976/25

scale :: Double -> [String] -> [String]
scale coef = map (\x -> if x =~ regex then (scaleLine coef x) else x)

scaleLine :: Double -> String -> String
scaleLine coef line = (sc from) ++ " --> " ++ (sc to)
    where
        from = take 12 line
        to = take 12 $ drop 17 $ line
        sc s = (writeTime . (*coef) . parseTime) s

parseTime :: String -> Double
parseTime s = hours * 3600 + mins * 60 + secs + (millis / 1000)
    where
        hours  = read $ take 2 $ s
        mins   = read $ take 2 $ drop 3 $ s
        secs   = read $ take 2 $ drop 6 $ s
        millis = read $ take 3 $ drop 9 $ s

writeTime :: Double -> String
writeTime t = (wZeros2 hours) ++ ":" ++ (wZeros2 mins) ++ ":" ++
                (wZeros2 secs) ++ "," ++ (wZeros3 millis)
    where
        trun = truncate t
        hours = trun `div` 3600
        mins = (trun `mod` 3600) `div` 60
        secs = trun `mod` 60
        millis = round $ (t - (fromIntegral trun)) * 1000

wZeros2 :: Integer -> String
wZeros2 n = if n < 10 then "0" ++ (show n) else (show n) 

wZeros3 :: Integer -> String
wZeros3 n = if n < 10 then "0" ++ (show n)
        else (if n < 100 then "0" ++ (show n) else (show n)) 